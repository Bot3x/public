import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TextInput,
  TouchableOpacity,
  DeviceEventEmitter,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      newNama: '',
      newAlamat: '',
      newdata: [],
    };
  }

  async componentDidMount() {
    await this.getData();
  }

  getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('data');
      this.setState({
        newdata: jsonValue != null ? JSON.parse(jsonValue) : [],
      });
    } catch (e) {
      // error reading value
    }
  };

  storeData = async () => {
    const {newNama, newAlamat} = this.state; //distructuring
    const json = [...this.state.newdata, {nama: newNama, alamat: newAlamat}]; //spreadoperator
    // console.log(json)
    try {
      const jsonValue = JSON.stringify(json);
      await AsyncStorage.setItem('data', jsonValue);
      await this.getData();
    } catch (e) {}
  };

  render() {
    console.log(this.state.newdata);
    return (
      <ScrollView>
        <View style={styles.container}>
          <View
            style={{
              backgroundColor: 'black',
              borderRadius: 10,
              marginTop: 20,
              alignItems: 'center',
            }}>
            <TextInput
              style={styles.input}
              placeholder="Nama"
              onChangeText={input => this.setState({newNama: input})}
              value={this.state.newNama}
            />
            <TextInput
              style={styles.input}
              placeholder="Alamat"
              onChangeText={input => this.setState({newAlamat: input})}
              value={this.state.newAlamat}
            />
            <Button title="Tambah" onPress={this.storeData} />
          </View>
          <View style={styles.table}>
            <View style={styles.inTable}>
              <Text style={styles.nama}>Nama</Text>
              <Text style={styles.alamat}>Alamat</Text>
            </View>
            {this.state.newdata.map((value, index) => {
              return (
                <View key={index} style={styles.onTable}>
                  <Text style={[styles.newNama, styles.data]}>
                    {value.nama}
                  </Text>
                  <Text style={[styles.newAlamat, styles.data]}>
                    {value.alamat}
                  </Text>
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
  },
  table: {
    backgroundColor: 'white',
    width: '90%',
    marginHorizontal: 20,
  },
  inTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'yellow',
    borderWidth: 2,
    height: 60,
    alignItems: 'center',
    borderRadius: 5,
  },
  onTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 60,
    borderWidth: 1,
    alignItems: 'center',
    borderRadius: 10,
  },
  name: {
    fontSize: 20,
  },
  address: {
    fontSize: 20,
  },
  number: {
    fontSize: 20,
  },
  input: {
    borderWidth: 2,
    width: '50%',
    marginHorizontal: 20,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: 'white',
  },
});
