import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TextInput,
  TouchableOpacity,
  DeviceEventEmitter,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Entypo';

export default class Crudtable extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        //Objek dalam array
        {id: 1, nama: 'Arif', alamat: 'JKT'},
        {id: 2, nama: 'Yono', alamat: 'BRB'},
        {id: 3, nama: 'Yini', alamat: 'BNG'},
        {id: 4, nama: 'Nino', alamat: 'MDN'},
        {id: 5, nama: 'Joko', alamat: 'BKS'},
        {id: 6, nama: 'Bodo', alamat: 'BGR'},
        {id: 7, nama: 'Dono', alamat: 'BL'},
        {id: 9, nama: 'Dino', alamat: 'KLP'},
        {id: 10, nama: 'Kano', alamat: 'BTL'},
        {id: 11, nama: 'Yori', alamat: 'KNT'},
        {id: 12, nama: 'Koco', alamat: 'NNT'},
        {id: 13, nama: 'Bado', alamat: 'NNB'},
        {id: 14, nama: 'Keno', alamat: 'NNU'},
      ],
      id: '',
      nama: '',
      alamat: '',
      onEdit: false,
    };
  }

  submit = () => {
    const {id, nama, alamat, data} = this.state; //Distructor
    const tampungan = data;
    tampungan.push({
      id: id,
      nama: nama,
      alamat: alamat,
    });
    this.setState({
      data: tampungan,
      id: '',
      nama: '',
      alamat: '',
    });
  };
  hapus = id => {
    this.setState(prevState => ({
      data: prevState.data.filter(value => value.id != id),
    }));
  };

  edit = value => {
    this.setState({
      id: value.id,
      nama: value.nama,
      alamat: value.alamat,
      onEdit: true,
    });
  };

  update = () => {
    const {id, nama, alamat} = this.state;
    this.setState(prevState => ({
      data: prevState.data.map(value => {
        if (value.id == id) {
          (value.id = id), (value.nama = nama), (value.alamat = alamat);
          return value;
        } else {
          return value;
        }
      }),
      onEdit: false,
      id: '',
      nama: '',
      alamat: '',
    }));
  };

  pilih = () => {
    this.setState({onSelect: !this.state.onSelect});
  };

  check = id => {
    this.setState(prevState => ({
      data: prevState.data.map(value => {
        if (value.id == id) {
          value.checked = !value.checked;
          return value;
        } else {
          return value;
        }
      }),
    }));
  };

  deleteSelected = () => {
    this.setState(prevState => ({
      data: prevState.data.filter(value => !value.checked),
      onSelect: false,
    }));
  };

  render() {
    const {data, id, nama, alamat, onEdit} = this.state;
    return (
      <ScrollView>
        <View style={styles.container}>
          <View
            style={{
              backgroundColor: 'black',
              borderRadius: 10,
              marginTop: 20,
              alignItems: 'center',
            }}>
            <TextInput
              style={styles.input}
              placeholder="Id"
              onChangeText={input => this.setState({id: input})}
              value={id}
            />
            <TextInput
              style={styles.input}
              placeholder="Nama"
              onChangeText={input => this.setState({nama: input})}
              value={nama}
            />
            <TextInput
              style={styles.input}
              placeholder="Alamat"
              onChangeText={input => this.setState({alamat: input})}
              value={alamat}
            />
            {onEdit ? (
              <Button title="Update" onPress={this.update} />
            ) : (
              <Button title="Submit" onPress={this.submit} />
            )}
            <Button title="Delete Selected" onPress={this.deleteSelected} />
          </View>
          <View style={styles.table}>
            <View style={styles.inTable}>
              <Text style={styles.number}>#</Text>
              <Text style={styles.nama}>Nama</Text>
              <Text style={styles.alamat}>Alamat</Text>
              <Text style={styles.hapus}>Action</Text>
            </View>
            {data.map((value, index) => {
              return (
                <View key={index} style={styles.onTable}>
                  <Text>{value.id}</Text>
                  <Text>{value.nama}</Text>
                  <Text>{value.alamat}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.check(value.id);
                    }}>
                    <Icon
                      name={value.checked ? 'emoji-happy' : 'emoji-sad'}
                      size={20}
                      color={value.checked ? 'red' : 'black'}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.hapus(value.id)}>
                    <Icon name="trash" size={20} color="black" />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.edit(value)}>
                    <Icon name="new-message" size={20} color="black" />
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
    justifyContent: 'center',
  },
  table: {
    backgroundColor: 'blue',
    width: '90%',
    marginHorizontal: 20,
  },
  inTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'red',
    borderWidth: 2,
    height: 60,
    alignItems: 'center',
    borderRadius: 5,
  },
  onTable: {
    justifyContent: 'space-around',
    height: 40,
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  input: {
    borderWidth: 2,
    width: '50%',
    marginHorizontal: 20,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: 'white',
  },
});

// import React, {Component} from 'react';
// import {
//   Text,
//   View,
//   StyleSheet,
//   Button,
//   TextInput,
//   TouchableOpacity,
//   DeviceEventEmitter,
// } from 'react-native';
// import Icon from 'react-native-vector-icons/Entypo';

// export default class Crudtable extends Component {
//   constructor() {
//     super();
//     this.state = {
//       data: [
//         {id: 1, nama: 'Arif', alamat: 'Jakarta'},
//         {id: 2, nama: 'Yono', alamat: 'Jawa'},
//         {id: 3, nama: 'Yini', alamat: 'Brebes'},
//       ],
//       newId: '',
//       newNama: '',
//       newAlamat: '',
//     };
//   }

//   tambah = () => {
//     const tampungan = this.state.data;
//     tampungan.push({
//       id: this.state.newId,
//       nama: this.state.newNama,
//       alamat: this.state.newAlamat,
//     });
//     this.setState({
//       data: tampungan,
//       newId: '',
//       newNama: '',
//       newAlamat: '',
//     });
//   };
//   hapus = penghapusan => {
//     const aksiHapus = this.state.data.filter(data => data.id != penghapusan.id);
//     this.setState({
//       data: aksiHapus,
//     });
//   };

//   render() {
//     return (
//       <View style={styles.container}>
//         <View
//           style={{
//             backgroundColor: 'black',
//             borderRadius: 10,
//             marginTop: 20,
//             alignItems: 'center',
//           }}>
//           <TextInput
//             style={styles.input}
//             placeholder="Id"
//             onChangeText={input => this.setState({newId: input})}
//             value={this.state.newId}
//           />
//           <TextInput
//             style={styles.input}
//             placeholder="Nama"
//             onChangeText={input => this.setState({newNama: input})}
//             value={this.state.newNama}
//           />
//           <TextInput
//             style={styles.input}
//             placeholder="Alamat"
//             onChangeText={input => this.setState({newAlamat: input})}
//             value={this.state.newAlamat}
//           />
//           <Button title="Tambah" onPress={this.tambah} />
//         </View>
//         <View style={styles.table}>
//           <View style={styles.inTable}>
//             <Text style={styles.number}>#</Text>
//             <Text style={styles.nama}>Nama</Text>
//             <Text style={styles.alamat}>Alamat</Text>
//             <Text style={styles.delete}>Action</Text>
//           </View>
//           {/* {this.state.data.map((value, index) => {
//             return (
//               <View key={index} style={styles.onTable}>
//                 <Text style={[styles.number, styles.data]}>{index + 1}</Text>
//                 <Text style={[styles.nama, styles.data]}>{value.nama}</Text>
//                 <Text style={[styles.alamat, styles.data]}>{value.alamat}</Text>
//                 <TouchableOpacity onPress={() => this.hapus(value)}>
//                   <Icon name="trash" size={30} color="black" />
//                 </TouchableOpacity>
//               </View>
//             );
//           })} */}
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: 'black',
//     flex: 1,
//     justifyContent: 'center',
//   },
//   table: {
//     backgroundColor: 'blue',
//     width: '90%',
//     marginHorizontal: 20,
//   },
//   inTable: {
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//     backgroundColor: 'red',
//     borderWidth: 2,
//     height: 60,
//     alignItems: 'center',
//     borderRadius: 5,
//   },
//   onTable: {
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//     height: 60,
//     borderWidth: 1,
//     alignItems: 'center',
//     borderRadius: 10,
//   },
//   name: {
//     fontSize: 20,
//   },
//   address: {
//     fontSize: 20,
//   },
//   number: {
//     fontSize: 20,
//   },
//   input: {
//     borderWidth: 2,
//     width: '50%',
//     marginHorizontal: 20,
//     marginTop: 20,
//     borderRadius: 5,
//     backgroundColor: 'white',
//   },
// });
