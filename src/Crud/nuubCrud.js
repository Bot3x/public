import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export class App extends Component {
  constructor() {
    super();
    this.state = {
      newData: '',
      newAddress: '',
      databaru: [],
    };
  }
  async componentDidMount() {
    await this.getData();
  }

  getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('data');
      this.setState({
        databaru: jsonValue != null ? JSON.parse(jsonValue) : null,
      });
    } catch (e) {
      // error reading value
    }
  };

  storeData = async () => {
    const {newData, newAddress} = this.state;
    const json = [...this.state.databaru, {newData, newAddress}];
    console.log(json);
    try {
      const jsonValue = JSON.stringify(json);
      await AsyncStorage.setItem('data', jsonValue);
      await this.getData();
    } catch (e) {}
  };

  render() {
    const {showPassword} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.table}>
          <View style={styles.inTable}>
            <Text style={styles.number}>#</Text>
            <Text style={styles.name}>Name</Text>
            <Text style={styles.address}>Address</Text>
          </View>
          {this.state.databaru.map((value, index) => {
            return (
              <View key={index} style={styles.onTable}>
                <Text style={[styles.name, styles.data]}>{value.newData}</Text>
                <Text style={[styles.address, styles.data]}>
                  {value.newAddress}
                </Text>
              </View>
            );
          })}
        </View>
        <View
          style={{
            backgroundColor: '#00BFFF',
            borderRadius: 10,
            marginTop: 20,
            alignItems: 'center',
          }}>
          <TextInput
            style={styles.input}
            placeholder="namapanggil"
            onChangeText={input => this.setState({newData: input})}
            value={this.state.newData}
          />
          <TextInput
            style={styles.input}
            placeholder="Address"
            onChangeText={input => this.setState({newAddress: input})}
            value={this.state.newAddress}
          />
          <Button title="Tambah" onPress={this.storeData} />
        </View>
      </View>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
  },
  table: {
    backgroundColor: '#ADD8E6',
    width: '90%',
    marginHorizontal: 20,
  },
  inTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#728FCE',
    borderWidth: 2,
    height: 60,
    alignItems: 'center',
    borderRadius: 5,
  },
  onTable: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 60,
    borderWidth: 1,
    alignItems: 'center',
    borderRadius: 5,
  },
  name: {
    fontSize: 20,
  },
  address: {
    fontSize: 20,
  },
  number: {
    fontSize: 20,
  },
  input: {
    borderWidth: 2,
    width: '50%',
    marginHorizontal: 20,
    marginTop: 20,
    borderRadius: 5,
    backgroundColor: 'white',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
