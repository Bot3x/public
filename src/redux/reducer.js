// const initialState = {
//   user: {},
// };

// const reducer = (state = initialState, action) => {
//   switch (action.type) {
//     case 'USER-DATA':
//       return {
//         ...state,
//         user: action.payload,
//       };
//     default:
//       return state;
//   }
// };

// export default reducer;

const initialState = {
  title: 'Redux',
  description: 'Learning State Management With Redux',
  students: [
    {id: 1, name: 'Arif'},
    {id: 2, name: 'Joni'},
    {id: 3, name: 'Dono'},
    {id: 4, name: 'Koki'},
  ],
  students: [],
  pengguna: [
    {username: 'Arif', password: '1234'},
    {username: 'Ario', password: '1234'},
  ],
  //type data boolean
  isLogin: false,
  currentUser: 'Default',
};
const reducer = (state = initialState, action) => {
  //condisional operator
  switch (action.type) {
    case 'DELETE-DATA':
      return {
        ...state,
        student: state.student.filter(item => item.id != action.payload), //funcion
      };
    case 'CHANGE-TITLE':
      return {
        ...state,
        title: action.payload,
      };
    case 'FIREBASE':
      return {
        ...state,
        Student: action.payload,
      };
    case 'ADD-DATA':
      return {
        ...state,
        pengguna: [...state.pengguna, action.payload],
      };
    case 'USER-DATA':
      return {
        ...state,
        pengguna: action.payload,
      };
    case 'USER-LOGIN':
      return {
        ...state,
        isLogin: action.payload,
      };
    case 'CURRENT-USER':
      return {
        ...state,
        currentUser: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
