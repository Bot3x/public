import React, {Component} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Alert,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

export class Inbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inbox: [
        {
          title: '',
          body: '',
        },
      ],
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('inbox')
      .then(resultInbox => {
        const inbox = JSON.parse(resultInbox);
        console.log(resultInbox);
        if (resultInbox) {
          this.setState({
            inbox: inbox,
          });
        }
      })
      .catch(err => {});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}></View>
        <ScrollView>
          <Text style={{color: 'white', fontSize: 50, fontWeight: 'bold'}}>
            Inbox
          </Text>
          {this.state.inbox.map((value, index) => (
            <View
              key={index}
              style={{
                width: '100%',
                height: 80,
                backgroundColor: 'white',
                borderWidth: 1,
                padding: 10,
              }}>
              <Text style={{fontWeight: 'bold', fontSize: 20}}>
                {value.title}
              </Text>
              <Text>{value.body}</Text>
            </View>
          ))}
        </ScrollView>
      </View>
    );
  }
}
export default Inbox;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue',
    justifyContent: 'center',
    borderWidth: 1,
  },
});
