import React, {Component} from 'react';
import {View, Text} from 'react-native';
import firestore from '@react-native-firebase/firestore';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        uid: 'default uid',
        nama: 'default name',
      },
      listRealTime: [],
      listOneTime: [],
    };
  }
  componentDidMount() {
    //onetime change
    firestore()
      .collection('Users')
      .get()
      .then(value => {
        let tampungan = value.docs.map(result => {
          return result.data();
        });

        this.setState({
          listOneTime: tampungan,
        });
      });

    //real time change
    firestore()
      .collection('Users')
      .onSnapshot(value => {
        let tampungan = value.docs.map(result => {
          return result.data();
        });
        this.setState({
          listRealTime: tampungan,
        });
      });
  }
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text style={{fontWeight: 'bold'}}>REAL TIME</Text>
        {this.state.listRealTime.map((value, index) => {
          return <Text key={index}> {value.nama}</Text>;
        })}
        <Text style={{fontWeight: 'bold', marginTop: 10}}>One TIme</Text>
        {this.state.listOneTime.map((value, index) => {
          return <Text key={index}> {value.nama}</Text>;
        })}
      </View>
    );
  }
}
