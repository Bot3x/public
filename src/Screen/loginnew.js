import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  ImageBackground,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export class loginnew extends Component {
  constructor() {
    super();
    this.state = {
      showPassword: true,
      Email: '',
      Password: '',
    };
  }
  submit = () => {
    const {Email, Password} = this.state;
    auth()
      .signInWithEmailAndPassword(Email, Password)
      .then(() => {
        alert('Login Succes');
        this.props.navigation.replace('dashboard');
      })
      .catch(error => {
        if (error.code === 'auth/user-not-found') {
          alert('User Not Found');
        } else if (error.code === 'auth/wrong-password') {
          alert('Wrong Password!');
        } else if (error.code === 'auth/invalid-email') {
          alert('Email addres is invalid');
        }
      });
  };

  show = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    const {showPassword} = this.state;
    const {Email, Password} = this.state;
    return (
      <ImageBackground
        style={styles.logo}
        source={{
          uri: 'https://i.pinimg.com/originals/55/d8/31/55d831b3503e6414e02f0fff55311ddb.jpg',
        }}>
        <View style={styles.container}>
          {/* <Text>{JSON.stringify(this.props.isLogin)}</Text> */}
          <View style={styles.input}>
            <Icon name="user" size={30} color="black" />
            <TextInput
              value={Email}
              placeholder="Username"
              onChangeText={typing => this.setState({Email: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="key" size={30} color="black" />
            <TextInput
              value={Password}
              secureTextEntry={showPassword}
              placeholder="Password"
              onChangeText={typing => this.setState({Password: typing})}
              style={{flex: 1}}
            />
            <TouchableOpacity onPress={this.show}>
              <Icon name="eye" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <Button title="Login" onPress={this.submit} />
          <Button
            title="SignUp"
            onPress={() => this.props.navigation.navigate('email')}
          />
        </View>
      </ImageBackground>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.userData,
    isLogin: state.isLogin,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isLogin: data =>
      dispatch({
        type: 'USER-DATA',
        payload: data,
      }),
    currentUser: data =>
      dispatch({
        type: 'USER-LOGIN',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(loginnew);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 300,
    backgroundColor: 'white',
    borderRadius: 20,
    marginTop: 20,
    paddingHorizontal: 8,
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  botton: {
    marginTop: 30,
    backgroundColor: 'pink',
    borderRadius: 10,
    width: 100,
    alignItems: 'center',
    elevation: 10,
    width: 300,
  },
});
