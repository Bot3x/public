// import React, {Component} from 'react';
// import { StyleSheet,Text,View,TextInput,TouchableOpacity,Image } from 'react-native';
// import Icon from 'react-native-vector-icons/AntDesign'
// import { connect } from 'react-redux'

// class signUp extends Component{
//     constructor() {
//       super()
//       this.state = {
//         showPassword:true,
//         username:'',
//         password:'',
//       }
//     }

// submit = () => {
//     const { username, password } = this.state;
//     const { addDataUser } = this.props;
//     addDataUser({username: username, password:password});
//     this.setState({
//         username:'',
//         password:'',

//     })
// }

// show = () => {
//     this.setState({
//         showPassword: !this.state.showPassword
//     })
// }

// render() {
//     const {showPassword} = this.state
//     return (
//        <View style={styles.container}>
//            <Text>
//                {JSON.stringify(this.props.user)}
//            </Text>
//            <Image style={styles.logo} source={{uri:''}}/>
//            <Text style={{fontSize:25,fontWeight:'bold'}}> Please SignUp First </Text>
//            <View style={styles.input}>
//                <Icon name= 'user' size={30} color = 'black' />
//                <TextInput value={this.state.username} placeholder='Username' onChangeText={(typing) =>this.setState({username:typing})}/>
//            </View>
//            <View style={styles.input}>
//                <Icon name= 'mail' size={30} color = 'black' />
//                <TextInput value={this.state.email} placeholder='Email' onChangeText={(typing) =>this.setState({email:typing})}/>
//            </View>
//            <View style={styles.input} >
//                <Icon name='eyeo' size={30} color='black' />
//                <TextInput value={this.state.password} secureTextEntry={showPassword} placeholder='Password' style={{flex: 1 }} onChangeText

//            </View>
//        </View>
//      )
// }

// }

//From Ario

// import * as React from 'react';
// import {
//   Alert,
//   Button,
//   TextInput,
//   View,
//   StyleSheet,
//   Image,
//   Text,
//   TouchableOpacity,
// } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome5';
// import firestore from '@react-native-firebase/firestore';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import {connect} from 'react-redux';
// import logo from '../assets/back.jpg';

// class RegisterRedux extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       username: '',
//       password: '',
//     };
//   }

//   submit = () => {
//     const {username, password} = this.state;
//     const {addDataUser} = this.props;
//     addDataUser({username: username, password: password});
//     this.setState({
//       username: '',
//       password: '',
//     });
//   };

//   render() {
//     const {navigation, user} = this.props;
//     const {username, password} = this.state;
//     return (
//       <View style={styles.container}>
//         <Image
//           style={{width: '100%', height: 100, marginBottom: 40}}
//           source={logo}
//         />
//         <Text style={{color: 'white'}}>{JSON.stringify(user)}</Text>
//         <Text style={{color: 'white', fontSize: 17}}>
//           {' '}
//           Please login with a registered session{' '}
//         </Text>
//         <View
//           style={{
//             flex: 0.1,
//             justifyContent: 'center',
//             alignItems: 'center',
//           }}></View>
//         <View style={styles.input}>
//           <Icon name="user" size={22} color="#000000" />
//           <TextInput
//             value={username}
//             onChangeText={user1 => this.setState({username: user1})}
//             placeholder={'Username'}
//             placeholderTextColor={'black'}
//             color={'black'}
//           />
//         </View>

//         <View style={styles.input}>
//           <Icon name="key" size={20} color="#000000" />
//           <TextInput
//             value={password}
//             onChangeText={pw1 => this.setState({password: pw1})}
//             placeholder={'Password'}
//             placeholderTextColor={'black'}
//             color={'black'}
//           />
//         </View>
//         <TouchableOpacity>
//           <Text
//             style={{color: 'blue', fontSize: 20, fontWeight: 'bold'}}
//             title="Login"
//             onPress={() => this.submit()}>
//             Login
//           </Text>
//         </TouchableOpacity>
//         <Text>{this.state.username}</Text>
//         <Text>{this.state.password}</Text>
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => {
//   // ambil dari reducer
//   return {
//     user: state.userData,
//   };
// };

// const mapDispatchToProps = dispatch => {
//   //ngirim  dari reducer
//   return {
//     addDataUser: data =>
//       dispatch({
//         type: 'ADD-DATA',
//         payload: data,
//       }),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(RegisterRedux);

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor: '#080E38',
//   },
//   input: {
//     width: 200,
//     height: 60,
//     padding: 10,
//     flexDirection: 'row',
//     borderWidth: 1,
//     borderColor: 'black',
//     elevation: 10,
//     backgroundColor: '#ecf0f1',
//     marginBottom: 20,
//     borderRadius: 17,
//   },
// });

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {TapGestureHandler} from 'react-native-gesture-handler';
import {connect} from 'react-redux';

class SignUp extends Component {
  constructor() {
    super();
    this.state = {
      showPassword: true,
      username: '',
      password: '',
    };
  }

  submit = () => {
    const {username, password} = this.state;
    const {addDataUser} = this.props;
    addDataUser({username: username, password: password});
    this.setState({
      username: '',
      password: '',
    });
  };

  show = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };
  // buttonPress= ()=>{
  //   this.setState({
  //       username:'',
  //       email:'',
  //       password:'',
  //       phone:''

  //   })
  // }
  render() {
    const {showPassword} = this.state;
    return (
      <ImageBackground
        style={styles.logo}
        source={{
          uri: 'https://i.pinimg.com/originals/55/d8/31/55d831b3503e6414e02f0fff55311ddb.jpg',
        }}>
        <View style={styles.container}>
          {/* <Text>{JSON.stringify(this.props.user)}</Text> */}
          <Text style={{fontSize: 25, fontWeight: 'bold'}}>
            Please SignUp First!
          </Text>
          <View style={styles.input}>
            <Icon name="user" size={30} color="black" />
            <TextInput
              value={this.state.username}
              placeholder="Username"
              onChangeText={typing => this.setState({username: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="mail" size={30} color="black" />
            <TextInput
              value={this.state.email}
              placeholder="Email"
              onChangeText={typing => this.setState({email: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="eyeo" size={30} color="black" />
            <TextInput
              value={this.state.password}
              secureTextEntry={showPassword}
              placeholder="Password"
              style={{flex: 1}}
              onChangeText={typing => this.setState({password: typing})}
            />
            <TouchableOpacity onPress={this.show}>
              <Icon name="eye" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <View style={styles.input}>
            <Icon name="phone" size={30} color="black" />
            <TextInput
              value={this.state.phone}
              placeholder="Phone Number"
              onChangeText={typing => this.setState({phone: typing})}
            />
          </View>
          <TouchableOpacity
            onPress={this.submit}
            style={{
              borderWidth: 2,
              borderColor: 'black',
              width: 300,
              alignItems: 'center',
              backgroundColor: '#6495ED',
              marginTop: 10,
              borderRadius: 10,
            }}>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 20}}>
              SignUp
            </Text>
          </TouchableOpacity>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{fontSize: 20, color: 'black', fontWeight: '600'}}>
              Already SignUp?
            </Text>
            <TouchableOpacity>
              <Text
                style={{color: 'blue', fontWeight: 'bold', fontSize: 20}}
                title="Go Login"
                onPress={() => this.props.navigation.goBack()}>
                Login{' '}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const mapStateToProps = state => {
  // ambil dari reducer
  return {
    user: state.pengguna,
  };
};

const mapDispatchToProps = dispatch => {
  //ngirim  dari reducer
  return {
    addDataUser: data =>
      dispatch({
        type: 'ADD-DATA',
        payload: data,
      }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 300,
    borderWidth: 3,
    backgroundColor: 'white',
    borderRadius: 20,
    marginTop: 20,
    paddingHorizontal: 8,
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
});
