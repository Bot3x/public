import React, {Component, version} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

class chatscreen extends Component {
  constructor() {
    super();
    this.state = {
      messageChat: [{message: 'oh yaudah bing ntar gua cariin rumput buat lu'}],
      newMessage: '',
    };
  }

  _send = () => {
    const {messageChat, newMessage} = this.state;
    const tampungan = messageChat;
    tampungan.push({
      message: newMessage,
    });
    this.setState({
      messageChat: tampungan,
      newMessage: '',
    });
  };

  render() {
    const {messageChat, newMessage} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.mainHeader}>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => navigation.navigate('kontakk')}>
              <Icon name="arrowleft" size={25} style={{color: '#000f63'}} />
            </TouchableOpacity>
            <Image
              source={{
                uri: 'https://4.bp.blogspot.com/-z579I96sqUQ/VMCrtKaUZ4I/AAAAAAAAABs/SYqzjvhafHg/s1600/bejo%2Bjual%2Bkambing.jpg',
              }}
              style={styles.photo}
            />
            <Text style={{color: '#000f63', fontSize: 20, fontWeight: 'bold'}}>
              {' '}
              Kambing
            </Text>
          </View>

          <View style={styles.header2}>
            <Icon name="videocamera" size={25} style={{color: '#000f63'}} />
            <Icon name="phone" size={25} style={{color: '#000f63'}} />
            <Icon name="ellipsis1" size={30} style={{color: '#000f63'}} />
          </View>
        </View>

        <ScrollView style={{padding: 10}}>
          <View style={styles.box}>
            <View style={styles.buble}>
              <Text style={{color: 'black', fontSize: 15}}>
                Cok help gua dong laper bet gua{' '}
              </Text>
            </View>
          </View>

          {messageChat.map((value, index) => (
            <View style={styles.box2}>
              <View style={styles.buble}>
                <Text style={{color: 'black', fontSize: 15}}>
                  {value.message}
                </Text>
              </View>
            </View>
          ))}
        </ScrollView>

        <View style={styles.footer}>
          <View style={styles.messageInput}>
            <TouchableOpacity>
              <Icon name="smileo" size={30} style={{color: '#000f63'}} />
            </TouchableOpacity>
            <TextInput
              style={{
                color: 'black',
                flex: 1,
                paddingHorizontal: 20,
                fontSize: 18,
              }}
              placeholder="Type message here...."
              value={newMessage}
              onChangeText={typing => this.setState({newMessage: typing})}
            />
          </View>

          <TouchableOpacity style={styles.send} onPress={this._send}>
            <Icon name="message1" size={30} style={{color: '#000f63'}} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default chatscreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#bac3e7',
  },
  mainHeader: {
    width: '100%',
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  header: {
    width: '50%',
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  header2: {
    width: '40%',
    height: 80,
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  footer: {
    width: '100%',
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    justifyContent: 'space-between',
  },
  messageInput: {
    width: '85%',
    height: 50,
    borderRadius: 30,
    borderColor: '#000f63',
    borderWidth: 0.5,
    backgroundColor: '#bac3e7',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  send: {
    width: 50,
    height: 50,
    borderRadius: 30,
    borderColor: '#000f63',
    borderWidth: 0.5,
    backgroundColor: '#bac3e7',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  photo: {
    width: 60,
    height: 60,
    borderRadius: 50,
    backgroundColor: '#000f63',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    backgroundColor: '#bac3e7',
    flex: 1,
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
  },
  box2: {
    backgroundColor: '#bac3e7',
    flex: 1,
    height: 60,
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  buble: {
    width: '70%',
    height: 50,
    borderRadius: 30,
    borderColor: '#000f63',
    borderWidth: 0.5,
    backgroundColor: '#bac3e7',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
  },
});

// // import React, {Component} from 'react';
// // import {
// //   ScrollView,
// //   StyleSheet,
// //   Text,
// //   TouchableOpacity,
// //   View,
// // } from 'react-native';
// // import {TextInput} from 'react-native-gesture-handler';
// // import Icon from 'react-native-vector-icons/AntDesign';
// // import {connect} from 'react-redux';
// // import firestore from '@react-native-firebase/firestore'
// // import Cheader from '../component/Cheader';

// // class chatscreen extends Component {
// //   constructor(props) {
// //     super(props);
// //     this.state = {
// //       target: this.props.route.params,
// //       messages: [],
// //       inputText: '',
// //     };
// //   }

// //   componentDidMount() {
// //     const {user} = this.props;
// //     const {target} = this.state;
// //     firestore()
// //       .collection('Users')
// //       .doc(target.uid)
// //       .onSnapshot(ress => {
// //         this.setState({
// //           target: ress.data(),
// //         });
// //       });

// //     firestore()
// //       .collection('Messages')
// //       .doc(user.uid)
// //       .collection('chatWith')
// //       .doc(target.uid)
// //       .onSnapshot(ress => {
// //         this.setState({
// //           messages: ress.data().messages,
// //         });
// //       });
// //   }

// //   send = () => {
// //     const {user} = this.props;
// //     const {target, inputText} = this.state;

// //     firestore()
// //       .collection('Messages')
// //       .doc(user.uid)
// //       .collection('chatWith')
// //       .doc(target.uid)
// //       .set(
// //         {
// //           messages: firestore.FieldValue.arrayUnion({
// //             text: inputText,
// //             sendBy: user.uid,
// //             date: new Date(),
// //           }),
// //         },
// //         {merge: true},
// //       )
// //       .then(() => {
// //         this.setState({inputLText: ''});
// //       });

// //     firestore()
// //       .collection('Messages')
// //       .doc(target.uid)
// //       .collection('chatWith')
// //       .doc(user.uid)
// //       .set(
// //         {
// //           messages: firestore.FieldValue.arrayUnion({
// //             text: inputText,
// //             sendBy: user.uid,
// //             date: new Date(),
// //           }),
// //         },
// //         {merge: true},
// //       );
// //   };

// //   render() {
// //     const {uid} = this.props.user;
// //     const {messages, inputText, target} = this.state;
// //     return(
// //       <View style={styles.container}>
// //        <Cheader target={target}/>
// //        <ScrollView style={{flex:1}} >
// //        {messages
// //        ?  messages.map((value, index, array)=>{
// //          let countTime = index !=0 ? convertDate(array[index-1].date.toDate()) : ''
// //          return(
// //            <View key={index} >
// //              {countTime != convertDate(value.date.toDate())
// //          && <CText style={styles.time}>{convertDate(value.date.toDate())}</CText>
// //              }
// //          )
// //        })
// //        }
// //        </ScrollView>
// //       </View>
// //     )
// //   }
// // }
