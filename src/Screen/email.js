import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export class email extends Component {
  constructor() {
    super();
    this.state = {
      showPassword: true,
      Email: '',
      password: '',
      alamat: '',
      noTelp: '',
      nama: '',
    };
  }

  _register = () => {
    const {Email, alamat, noTelp, nama, password} = this.state;
    auth()
      .createUserWithEmailAndPassword(Email, password)
      .then(response => {
        // console.log(response.user);
        firestore()
          .collection('Users')
          .doc(response.user.uid)
          .set({
            nama: nama,
            alamat: alamat,
            noTelp: noTelp,
            Email: Email,
            uid: response.user.uid,
          })
          .then(() => {
            alert('User account created & signed in !');
          });
      })
      .catch(error => {
        if (error.code === 'auth/email already in use') {
          alert('That email addres is already in use');
        }
        if (error.code === 'auth/invalid-email') {
          alert('That email addres is invalid');
        }
        console.error(error);
      });
  };

  show = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    const {navigation, user} = this.props;
    const {showPassword} = this.state;
    const {Email, alamat, noTelp, nama, password} = this.state;
    return (
      <ImageBackground
        style={styles.logo}
        source={{
          uri: 'https://i.pinimg.com/originals/55/d8/31/55d831b3503e6414e02f0fff55311ddb.jpg',
        }}>
        <View style={styles.container}>
          <View style={styles.input}>
            <Icon name="mail" size={30} color="black" />
            <TextInput
              value={Email}
              placeholder="Email"
              onChangeText={typing => this.setState({Email: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="home" size={30} color="black" />
            <TextInput
              value={alamat}
              placeholder="Alamat"
              onChangeText={typing => this.setState({alamat: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="phone" size={30} color="black" />
            <TextInput
              value={noTelp}
              placeholder="Nomor Telepon"
              onChangeText={typing => this.setState({noTelp: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="user" size={30} color="black" />
            <TextInput
              value={nama}
              placeholder="Nama"
              onChangeText={typing => this.setState({nama: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="key" size={30} color="black" />
            <TextInput
              value={password}
              secureTextEntry={showPassword}
              placeholder="Password"
              onChangeText={typing => this.setState({password: typing})}
              style={{flex: 1}}
            />
            <TouchableOpacity onPress={this.show}>
              <Icon name="eye" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <Button title="Register" onPress={this._register} />
          <Button
            title="Back to Login"
            onPress={() => this.props.navigation.navigate('login')}
          />
        </View>
      </ImageBackground>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.userData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDataUsers: data =>
      dispatch({
        type: 'ADD-DATA',
        payload: data,
      }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(email);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 300,
    backgroundColor: 'white',
    borderRadius: 20,
    marginTop: 20,
    paddingHorizontal: 8,
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  botton: {
    marginTop: 30,
    backgroundColor: 'pink',
    borderRadius: 10,
    width: 100,
    alignItems: 'center',
    elevation: 10,
    width: 300,
  },
});
