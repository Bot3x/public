import React,  { Component } from 'react'
import { Text, View, StyleSheet, Image, ScrollView, } from 'react-native'   
import firestore from '@react-native-firebase/firestore';

export class Kontak extends Component {
    constructor(){
        super()
        this.state = {


            onePiece: []
        }
    }

    componentDidMount(){
        firestore()
        .collection('Kontak')
        .onSnapshot((value)=> {
            let data = value.docs.map(result => {
                return result.data()
            // value.docs.forEach(result => {
            //     data.push(result.data())
            //     console.log(data)
            })
            this.setState({
                onePiece:data
            });
        });
    }
   
    render (){
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={{fontSize:25, color:'white', padding:10, textAlign:'center'}}>One Piece</Text>
                    {this.state.onePiece.map((value, index)=>(
                     <View key={index} style={{flex:1, flexDirection:'row', marginVertical:1, borderWidth:0.5,
                                               borderBottomColor:'#39A2DB', padding:10, backgroundColor:'black'}}>
                     <Image source={{uri:value.foto}} style={{width:80, height:80, borderRadius:10}}/>
                     <Text style={{flex:1, fontSize:20, padding:5, fontWeight:'bold', color:'white'}}>{value.nama}</Text>
                     <Text style={{color:'green'}}>Online</Text>
                     </View>
                    ))}
                </ScrollView>
            </View>
        )
    }
}
export default Kontak
const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        borderWidth:1,
        backgroundColor:'red'
    }
})
