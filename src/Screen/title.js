// import React, {Component} from 'react';
// import {View, Text, Button} from 'react-native';
// import {connect} from 'react-redux';

// class Reduxtest extends Component {
//   constructor() {
//     super();
//   }

//   handleDeleteData = id => {
//     this.props.deleteData(id);
//   };

//   render() {
//     return (
//       <View>
//         <Text> Title : {this.props.title}</Text>
//         <Text> Description : {this.props.description} </Text>
//         <Text>------------------------------------</Text>
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => {
//   return {
//     title: state.title,
//     description: state.description,
//   };
// };

// const mapDispatchToProps = Dispatch => {
//   return {
//     deleteData: id => dispatch(deleteData(id)),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Reduxtest);

// AMBIL DATA DARI FIRE BASE
import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Button} from 'react-native';
import {connect} from 'react-redux';
import CText from '../component/CText';
import CButton from '../component/CButton';
import firestore from '@react-native-firebase/firestore';

class Scren extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputUsername: '',
      inputPassword: '',
    };
  }

  _change = () => {
    this.props.changeTitle(this.state.input);
    this.setState({
      input: '',
    });
  };

  componentDidMount() {
    firestore()
      .collection('Users')
      .onSnapshot(value => {
        let tampungan = [];
        value.docs.forEach(result => {
          tampungan.push(result.data());
        });
        this.props.setStudent(tampungan);
      });
  }

  render() {
    const {user} = this.props;
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 20}}> {this.props.title}</Text>
        <View></View>
        <TextInput
          style={styles.input}
          placeholder="Ganti"
          onChangeText={typing => this.setState({input: typing})}
          value={this.state.input}
        />
        <Button title="Ganti" onPress={this._change}></Button>
        {user.map((value, index) => (
          <View key={index} style={{flexDirection: 'row'}}>
            <Text style={{marginVertical: 5, marginHorizontal: 5}}>
              {value.nama}
            </Text>
            <Text style={{marginVertical: 5, marginHorizontal: 5}}>
              {value.uid}
            </Text>
          </View>
        ))}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    title: state.title,
    user: state.students,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteData: data =>
      dispatch({
        type: 'CHANGE-TITLE',
        payload: data,
      }),
    setStudent: data =>
      dispatch({
        type: 'FIREBASE',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Scren);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#728FCE',
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 8,
    paddingHorizontal: 10,
    fontSize: 16,
    marginBottom: 10,
    elevation: 8,
  },
});

//MAHRUS PUNYA
// import React, {Component} from 'react';
// import {View, StyleSheet, Alert} from 'react-native';
// import {Colors} from '../../assets';
// import {connect} from 'react-redux';
// import {CButton, CText, CTextInput} from '../../component/';
// export class index extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       username: '',
//       password: '',
//       viewPassword: true,
//     };
//   }
//   _submit = () => {
//     const {users, navigation, currentUser, isLogin} = this.props;
//     const {username, password, user} = this.state;
//     const checkUserName = users.filter(users => users.name == username);
//     const currentUserName = checkUserName[0];

//     if (currentUserName !== undefined) {
//       if (password == currentUserName.password) {
//         currentUser(checkUserName);
//         isLogin(true);
//         navigation.replace('Dashboard');
//       } else {
//         Alert.alert('Data Invalid', `Email or Password did'nt Match`);
//       }
//     } else {
//       Alert.alert('Data Invalid', `Email or Password did'nt Match`);
//     }
//     // console.log(currentUser)
//   };

//   _register = () => {
//     this.props.navigation.navigate('Signup');
//   };

//   render() {
//     const {password, username} = this.state;

//     return (
//       <View style={styles.mainContainer}>
//         <CText style={styles.title}>Login</CText>

//         <CText style={styles.text}>
//           You don’t think you should login first and behave like human not
//           robot.
//         </CText>

//         <CTextInput
//           placeholder="Username"
//           style={styles.input}
//           onChangeText={typing => this.setState({username: typing})}
//         />

//         <CTextInput
//           onChangeText={typing => this.setState({password: typing})}
//           placeholder="Password"
//           style={styles.input}
//         />

//         <View style={styles.button}>
//           <CButton
//             onPress={() => {
//               this._submit();
//             }}>
//             Login
//           </CButton>
//         </View>

//         <CText style={styles.text}>
//           Forgot Password?{' '}
//           <CText
//             style={styles.link}
//             onPress={() => {
//               this._forgot();
//             }}>
//             Click Here !
//           </CText>
//         </CText>

//         <CText style={styles.text}>
//           New User?{' '}
//           <CText
//             style={styles.link}
//             onPress={() => {
//               this._register();
//             }}>
//             Register
//           </CText>
//         </CText>
//       </View>
//     );
//   }
// }
// const mapStateToProps = state => {
//   return {
//     users: state.users,
//     isLogin: state.isLogin,
//   };
// };
// const mapStateToDispatch = send => {
//   return {
//     isLogin: data =>
//       send({
//         type: 'USER-LOGIN',
//         payload: data,
//       }),
//     currentUser: data =>
//       send({
//         type: 'CURRENT-USER',
//         payload: data,
//       }),
//   };
// };

// export default connect(mapStateToProps, mapStateToDispatch)(index);

// const styles = StyleSheet.create({
//   mainContainer: {
//     flex: 1,
//     justifyContent: 'center',
//     backgroundColor: '#1e1e1e',
//   },
//   title: {
//     color: Colors.primary,
//     fontSize: 32,
//     paddingLeft: 15,
//     fontWeight: 'bold',
//   },
//   input: {
//     marginVertical: 10,
//   },
//   button: {
//     marginTop: 10,
//     alignSelf: 'center',
//   },
//   text: {
//     color: Colors.white,
//     textAlign: 'center',
//   },
//   link: {
//     color: Colors.secondary,
//     textDecorationLine: 'underline',
//   },
// });

// import React, {Component} from 'react';
// import { StyleSheet,Text,View,TextInput,TouchableOpacity,Image } from 'react-native';
// import Icon from 'react-native-vector-icons/AntDesign'
// import { connect } from 'react-redux'

// class Scren extends Component{
//   constructor() {
//     super()
//     this.state = {
//       showPassword:true,
//       Username:'',
//       Password:'',
//     }
//   }
//   submit = () => {
//     const { Username,Password} = this.state;
//     const {user , userLogin} = this.props;
//     const getUser = user.filter(value => value.username === Username),
//     const users = getUser[0];

//     if (users !== undefined){
//       if (users.Password === Password){
//         alert('Login Sukses');
//         userLogin(true);
//       } else {
//         alert('Login Fail')
//       }
//     }
//   }
// }

//  show=()=>{
//    this.setState({
//      showPassword: !this.state.showPassword
//    })
//  }

//  const mapStateToProps = (state) => {
//    return{
//      user:state.users
//    }
//  }

//  const mapDispatchToProps = (dispatch) => {
//    return {
//      addDataUser: (data) => dispatch({
//        type: 'ADD-DATA',
//        payload:data,
//      }),
//    }
//  }

//  export default connect(mapStateToProps, mapDispatchToProps)(Scren)
