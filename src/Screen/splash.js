import React, {Component} from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {connect} from 'react-redux';

export class SplashScreen extends Component {
  componentDidMount() {
    const {isLogin} = this.props;
    if (isLogin === true) {
      this.props.navigation.replace('dashboard');
    } else {
      this.props.navigation.replace('login');
    }
  }

  // componentDidMount() {
  //   auth().onAuthStateChanged(user => {
  //     setTimeout(() => {
  //       console.log(user);
  //       if (user) {
  //         this.props.navigation.replace('dashboard');
  //       } else {
  //         this.props.navigation.replace('login');
  //       }
  //     }, 3000);
  //   });
  // }
  //   constructor() {
  //     super();
  //     this.state = {};
  //   }

  //   async componentDidMount() {
  //     //
  //     const data = await this.getData();
  //     //   console.log(data)
  //     setTimeout(() => {
  //       if (data != null) {
  //         this.props.navigation.replace('screendash');
  //       } else {
  //         this.props.navigation.replace('loginnew');
  //       }
  //     }, 3000);
  //   }

  //   getData = async () => {
  //     try {
  //       const valueData = await AsyncStorage.getItem('data');
  //       return valueData != null ? JSON.parse(valueData) : null;
  //     } catch (error) {}
  //   };

  render() {
    return (
      <ImageBackground
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
        source={{
          uri: 'https://i.pinimg.com/originals/55/d8/31/55d831b3503e6414e02f0fff55311ddb.jpg',
        }}>
        {/* <ImageBackground style={{width:250,height:250}} source={{uri:''}}></ImageBackground> */}
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin,
  };
};
export default connect(mapStateToProps)(SplashScreen);
