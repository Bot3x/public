import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import {connect} from 'react-redux';

export class kontakk extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
    };
  }

  async componentDidMount() {
    firestore()
      .collection('Users')
      .where('uid', '!=', this.props.users.uid)
      .onSnapshot(value => {
        this.setState({
          users: value.docs.map(result => {
            return result.data();
          }),
        });
      });
  }
  render() {
    // console.log(this.state.users);
    const {users} = this.state;
    return (
      <View>
        {users.map((value, index) => (
          <View key={index} style={{borderWidth: 2, alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('chatscreen', {
                  name: value.nama,
                })
              }>
              <Text>{value.nama}</Text>
            </TouchableOpacity>
          </View>
        ))}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    Users: state.userData,
  };
};
export default connect(mapStateToProps)(kontakk);
