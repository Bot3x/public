import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firestore from '@react-native-firebase/firestore';
import {connect} from 'react-redux';

class App extends Component {
  constructor() {
    super();
    this.state = {showPassword: true};
  }

  // async componentDidMount() {
  //   const getUid = await auth().currentUser.uid;
  //   firestore()
  //     .collection('Users')
  //     .doc(getUid)
  //     .onSnapshot(Response => {
  //       this.props.setUser(Response.data());
  //     });
  // }

  // SignOut Redux
  _signOut = () => {
    const {isLogin} = this.props;
    // auth()
    // .signOut()
    // .then(() => this.props.navigation.replace('login'));
    isLogin(false);
    this.props.navigation.replace('login');
  };

  // _signOut = () => {
  //   auth()
  //     .signOut()
  //     .then(() => this.props.navigation.replace('login'));
  // };

  render() {
    const {user} = this.props;
    return (
      <View style={styles.assigment2}>
        <ImageBackground
          style={{flex: 1, alignItems: 'center', width: '100%', height: '100%'}}
          source={{
            uri: 'https://images.wallpapersden.com/image/download/monkey-luffy-4k_a21qZ2yUmZqaraWkpJRmZW1lrWZuZ2U.jpg',
          }}>
          <Text style={{fontSize: 25, fontWeight: 'bold', color: 'black'}}>
            HOME ONE PIECE{' '}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}></View>
          <View style={styles.Button}>
            <Button
              title="Crud"
              onPress={() => this.props.navigation.navigate('Crudtable')}
            />
          </View>
          <View style={styles.Button}>
            <Button
              title="FireStore"
              onPress={() => this.props.navigation.navigate('firestore')}
            />
          </View>
          <View style={styles.Button}>
            <Button
              title="Kontak"
              onPress={() => this.props.navigation.navigate('kontakk')}
            />
          </View>
          <Text>{JSON.stringify(user)}</Text>
          <Button title="SignOut" onPress={this._signOut} />
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: data =>
      dispatch({
        type: 'USER-DATA',
        payload: data,
      }),
    isLogin: data =>
      dispatch({
        type: 'USER-LOGIN',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
const styles = StyleSheet.create({
  assigment2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F0FFFF',
  },
  Button: {
    justifyContent: 'center',
    flex: 1,
  },
});
