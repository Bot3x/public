// FIRST
// import React, {Component} from 'react';
// import {
//   StyleSheet,
//   Text,
//   View,
//   TextInput,
//   TouchableOpacity,
//   Image,
//   ImageBackground,
// } from 'react-native';
// import Icon from 'react-native-vector-icons/AntDesign';
// import CButton from '../component/CButton';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import {connect} from 'react-redux';

// class Login extends Component {
//   constructor() {
//     super();
//     this.state = {
//       //   showPassword: true,
//       inputUsername: '',
//       inputPassword: '',
//     };
//   }

//   //   show = () => {
//   //     this.setState({
//   //       showPassword: !this.state.showPassword,
//   //     });
//   //   };

//   _login = () => {
//     const {inputUsername, inputPassword} = this.state;
//     const {user, userLogin} = this.props;
//     const getUser = user.filter(value => value.inputUsername === inputUsername);
//     const users = getUser[0];

//     if (users !== undefined) {
//       if (users.password === inputPassword) {
//         alert('Login Berhasil');
//         userLogin(true);
//       } else {
//         alert('Login Gagal');
//       }
//     }
//   };

//   render() {
//     const {navigation, isLogin} = this.props;
//     const {inputUsername, inputPassword} = this.state;
//     return (
//       <View style={styles.assigment2}>
//         <ImageBackground
//           style={{
//             flex: 1,
//             justifyContent: 'center',
//             alignItems: 'center',
//             width: '100%',
//             height: '100%',
//           }}
//           source={{uri: 'https://wallpaperaccess.com/full/222387.png'}}>
//           {/* <ImageBackground style={{width:250,height:250}} source={{uri:'https://www.kibrispdr.org/data/one-piece-png-hd-8.jpg'}}></ImageBackground> */}
//           <Text style={{fontSize: 25, fontWeight: 'bold'}}>ONE PIECE</Text>
//           <View style={styles.input}>
//             <Icon name="user" size={30} color="black" />
//             <TextInput
//               value={inputUsername}
//               placeholder="Username"
//               onChangeText={typing => this.setState({inputUsername: typing})}
//             />
//           </View>
//           <View style={styles.input}>
//             <Icon name="key" size={30} color="black" />
//             <TextInput
//               value={inputPassword}
//               //   secureTextEntry={showPassword}
//               placeholder="Password"
//               onChangeText={typing => this.setState({inputPassword: typing})}
//               style={{flex: 1}}
//             />
//           </View>
//           <CButton title="Login" onPress={this._login} />
//         </ImageBackground>
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => {
//   // ambil dari reducer
//   return {
//     user: state.userData,
//     isLogin: state.isLogin,
//   };
// };

// const mapDispatchToProps = dispatch => {
//   //ngirim  dari reducer
//   return {
//     getDataUsers: data =>
//       dispatch({
//         type: 'USER-DATA',
//         payload: data,
//       }),
//     userLogin: data =>
//       dispatch({
//         type: 'USER-LOGIN',
//         payload: data,
//       }),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Login);

// const styles = StyleSheet.create({
//   assigment2: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F0FFFF',
//   },
//   input: {
//     alignItems: 'center',
//     flexDirection: 'row',
//     paddingLeft: 25,
//     width: 300,
//     height: 50,
//     margin: 10,
//     borderWidth: 1,
//     borderRadius: 5,
//     borderColor: 'black',
//     backgroundColor: 'white',
//     elevation: 15,
//     paddingLeft: 15,
//     paddingHorizontal: 8,
//   },
//   logo: {
//     marginBottom: 20,
//     alignSelf: 'center',
//     width: 300,
//     height: 150,
//   },
//   background: {
//     flex: 1,
//     resizeMode: 'cover',
//     justifyContent: 'center',
//     alignItems: 'center',
//     position: 'absolute',
//     width: '100%',
//     height: '100%',
//   },
// });

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {connect} from 'react-redux';

export class reduxlogin extends Component {
  constructor() {
    super();
    this.state = {
      showPassword: true,
      Username: '',
      Password: '',
    };
  }
  submit = () => {
    const {Username, Password} = this.state;
    const {user, isLogin} = this.props;
    const getUser = user.filter(value => value.username === Username);
    const users = getUser[0];

    if (users !== undefined) {
      if (users.password === Password) {
        alert('Login Berhasil');
        this.props.navigation.replace('dashboard');
        isLogin(true);
      } else {
        alert('Login Gagal');
      }
    }
  };

  show = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };

  render() {
    // console.log(this.props.user);
    const {showPassword} = this.state;
    const {Username, Password} = this.state;
    return (
      <ImageBackground
        style={styles.logo}
        source={{
          uri: 'https://i.pinimg.com/originals/55/d8/31/55d831b3503e6414e02f0fff55311ddb.jpg',
        }}>
        <View style={styles.container}>
          <Text>{JSON.stringify(this.props.isLogin)}</Text>
          <View style={styles.input}>
            <Icon name="user" size={30} color="black" />
            <TextInput
              value={Username}
              placeholder="Username"
              onChangeText={typing => this.setState({Username: typing})}
            />
          </View>
          <View style={styles.input}>
            <Icon name="eyeo" size={30} color="black" />
            <TextInput
              value={Password}
              secureTextEntry={showPassword}
              placeholder="Password"
              onChangeText={typing => this.setState({Password: typing})}
              style={{flex: 1}}
            />
            <TouchableOpacity onPress={this.show}>
              <Icon name="eye" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <Button title="Login" onPress={this.submit} />
          <Button
            title="SignUp"
            onPress={() => this.props.navigation.navigate('signup')}
          />
        </View>
      </ImageBackground>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.pengguna,
    isLogin: state.isLogin,
    des: state.description,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    isLogin: data =>
      dispatch({
        type: 'USER-LOGIN',
        payload: data,
      }),
    currentUser: data =>
      dispatch({
        type: 'CURRENT-USER',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxlogin);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 300,
    backgroundColor: 'white',
    borderRadius: 20,
    marginTop: 20,
    paddingHorizontal: 8,
  },
  // logo: {
  //   marginTop: 40,
  //   marginBottom: 20,
  //   alignSelf: 'center',
  //   width: 200,
  //   height: 150,
  logo: {
    width: '100%',
    height: '100%',
  },
  botton: {
    marginTop: 30,
    backgroundColor: 'pink',
    borderRadius: 10,
    width: 100,
    alignItems: 'center',
    elevation: 10,
    width: 300,
  },
});
