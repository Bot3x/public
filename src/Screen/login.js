import  React,{Component} from 'react';
import {StyleSheet,Text,View,TextInput,TouchableOpacity,Image, ImageBackground} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import CButton from '../component/CButton';
import AsyncStorage from '@react-native-async-storage/async-storage'

export default class Login extends Component{
constructor(){
  super();
  this.state = {
   showPassword:true,
   Username:'',
   Password:''
  }
}

submit = async () => {
  const { Username, Password } = this.state
  if(Username != '' && Password != ''){

    const dataUser = {username:Username, password:Password}
    const jsonuser = JSON.stringify(dataUser)
    await AsyncStorage.setItem('data', jsonuser)

        this.props.navigation.navigate("dashboard")

  } else {
    alert('BRO ISI DULU NGAPA BRO')
  }
}

show=()=>{
  this.setState({
    showPassword: !this.state.showPassword
  })
}

// buttonPress= ()=>{
//   this.setState({
//     email:'',
//     password:'',
//   })
// }

  render() {
    const {showPassword} = this.state
    return (
      <View style={styles.assigment2}>
        <ImageBackground style={{flex:1,justifyContent:'center',alignItems:'center', width:'100%', height:'100%'}} source={{uri:'https://wallpaperaccess.com/full/222387.png'}}>
           {/* <ImageBackground style={{width:250,height:250}} source={{uri:'https://www.kibrispdr.org/data/one-piece-png-hd-8.jpg'}}></ImageBackground> */}
        <Text style={{fontSize:25,fontWeight:'bold'}}>ONE PIECE</Text> 
        <View style={styles.input}>
            <Icon name='user' size={30} color='black'/>
            <TextInput value={this.state.email} placeholder="Username"onChangeText ={(typing)=>this.setState({Username:typing})}/>
        </View>
        <View style={styles.input}>
            <Icon name='key' size={30} color='black'/>
            <TextInput value={this.state.password} secureTextEntry={showPassword} placeholder='Password'onChangeText ={(typing)=>this.setState({Password:typing})}style={{flex: 1}}/>
            <TouchableOpacity onPress={this.show}>
              <Icon name ='eye' size={30} color='black'/>
            </TouchableOpacity>
        </View>
          <CButton title="Login" onPress={this.submit} />
          </ImageBackground>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  assigment2: {
    flex:1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: "#F0FFFF"
  },
  input:{
    alignItems:'center',
    flexDirection:'row',
    paddingLeft: 25, 
    width:300,
    height: 50, 
    margin: 10, 
    borderWidth:1,
    borderRadius:5,
    borderColor:'black',
    backgroundColor:'white',
    elevation: 15,
    paddingLeft:15,
    paddingHorizontal:8,


  },
  logo:{
   
    marginBottom:20,
    alignSelf:'center',
    width:300,
    height:150,
},
  background:{
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
})