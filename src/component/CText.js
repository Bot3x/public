import * as React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
export default class Reduxtest extends React.Component {
  render() {
    return (
      <View style={styles.contrainer}>
        <TouchableOpacity style={styles.button} onPress={this.props.onPress}>
          <Text style={{color: 'white', textAlign: 'center'}}>BUTTON</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contrainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'black',
    padding: 10,
    width: '80%',
    borderRadius: 8,
    elevation: 4,
    marginStart: 5,
  },
});
