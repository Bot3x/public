import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import CText from '../component/CText';

class Cheader extends Component {
  render() {
    const {nama, isOnline} = this.props.target;
    return (
      <View style={styles.container}>
        <CText style={styles.title} bold>
          {' '}
          {nama.toUpperCase()}{' '}
        </CText>
        <CText style={styles.title} bold>
          {' '}
          {isOnline ? 'Online' : 'Offline'}
        </CText>
      </View>
    );
  }
}

export default Cheader;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingVertical: 16,
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
  },
});
