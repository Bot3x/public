import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginForm from '../Screen/login';
import DashBoard from '../Screen/screendash';
import SplashScreen from '../Screen/splash';
import messaging from '@react-native-firebase/messaging';
import Scren from '../Screen/title';
import Test from '../Screen/test';
import reduxlogin from '../Screen/reduxlogin';
import {Provider} from 'react-redux';
import {PersistGate, Store} from 'redux-persist';
import Crudtable from '../Crud/Crudtable';
import firestore from '../Crud/Crudfirestore';
import dashboard1 from '../dasboard/dashboard1';
// import kontak from '../Kontak';
import Inbox from '../Screen/inboxasync';
import email from '../Screen/email';
import loginnew from '../Screen/loginnew';
import kontakk from '../Screen/kontakk';
import chatscreen from '../Screen/chatscreen';
import reduxsignup from '../Screen/reduxsignup';
const Stack = createStackNavigator();

class Navigation extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="splash"
          screenOptions={{headerShown: false}}>
          {/* <Stack.Screen name="login" component={loginnew} /> */}
          {/* <Stack.Screen name="login" component={reduxlogin} />
          <Stack.Screen name="signup" component={reduxsignup} /> */}
          <Stack.Screen name="dashboard" component={DashBoard} />
          <Stack.Screen name="splash" component={SplashScreen} />
          {/* <Stack.Screen name="signup" component={signup} /> */}
          <Stack.Screen name="Crudtable" component={Crudtable} />
          <Stack.Screen name="firestore" component={firestore} />
          <Stack.Screen name="kontakk" component={kontakk} />
          <Stack.Screen name="chatscreen" component={chatscreen} />
          <Stack.Screen name="email" component={email} />
          {/* <Stack.Screen name="inbox" component={Inbox} /> */}
          {/* <Stack.Screen name="Scren" component={Scren} /> */}
          {/* <Stack.Screen name="Test" component={Test} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
export default Navigation;
