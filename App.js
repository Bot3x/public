// import React, {Component} from 'react';
// import {StatusBar} from 'react-native';
// import {Provider} from 'react-redux';
// import {NavigationContainer} from '@react-navigation/native';
// import Navigation from './src/navigation';
// import {createStackNavigator} from '@react-navigation/stack';
// import {PersistGate} from 'redux-persist/es/integration/react';
// import {Store, Persistor} from './src/redux/store';

// // import Router from './src/'

// class App extends Component {
//   render() {
//     return (
//       <Provider store={Store}>
//         <PersistGate loading={null} persistor={Persistor}>
//           <Navigation />
//         </PersistGate>
//       </Provider>
//     );
//   }
// }

// export default App;

import 'react-native-gesture-handler';
import React from 'react';
import type {Node} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';
import Inbox from './src/Screen/inbox';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();
const hide = {headerShown: false};

const App: () => Node = () => {
  messaging().onMessage(async remoteMessage => {
    Alert.alert(
      'Anda menerima BACOT!!',
      JSON.stringify(remoteMessage.notification.body),
    );
    const title = remoteMessage.notification.title;
    const body = remoteMessage.notification.body;
    AsyncStorage.getItem('inbox')
      .then(resultInbox => {
        const inbox = JSON.parse(resultInbox);
        AsyncStorage.setItem(
          'inbox',
          JSON.stringify([{title: title, body: body}, ...inbox]),
        );
      })
      .catch(err => {
        AsyncStorage.setItem(
          'inbox',
          JSON.stringify([{title: title, body: body}]),
        );
      });
  });

  messaging()
    .getToken()
    .then(token => console.log(token));

  // Register background handler
  messaging().setBackgroundMessageHandler(async remoteMessage => {
    const title = remoteMessage.notification.title;
    const body = remoteMessage.notification.body;
    AsyncStorage.getItem('inbox')
      .then(resultInbox => {
        const inbox = JSON.parse(resultInbox);
        AsyncStorage.setItem(
          'inbox',
          JSON.stringify([{title: title, body: body}, ...inbox]),
        );
      })
      .catch(err => {
        AsyncStorage.setItem(
          'inbox',
          JSON.stringify([{title: title, body: body}]),
        );
      });
    console.log('Message handled in the BACOT!', remoteMessage);
  });

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Inbox">
        <Stack.Screen name="Inbox" component={Inbox} options={hide} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
